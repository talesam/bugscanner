#!/usr/bin/env bash


# Alterar apenas a variável FRONT abaixo
FRONT="d1a7o1za3p7p6r.cloudfront.net"

### NÃO ALTERAR NADA AQUI PARA BAIXO

# Cores degrade
RED001='\e[38;5;1m'		# Vermelho 1
RED009='\e[38;5;9m'		# Vermelho 9
CYA122='\e[38;5;122m'		# Ciano 122
CYA044='\e[38;5;44m'		# Ciano 44
ROX063='\e[38;5;63m'		# Roxo 63
ROX027='\e[38;5;27m'		# Roxo 27
GRE046='\e[38;5;46m'		# Verde 46
GRY247='\e[38;5;247m'		# Cinza 247
LAR208='\e[38;5;208m'		# Laranja 208
LAR214='\e[38;5;214m'		# Laranja 214
AMA226='\e[38;5;226m'		# Amarelo 226
BLU039='\e[38;5;44m'		# Azul 39
MAR094='\e[38;5;94m'		# Marrom 94
MAR136='\e[38;5;136m'		# Marrom 136

# Cores chapadas
CIN='\e[30;1m'			# Cinza
RED='\e[31;1m'			# Vermelho
GRE='\e[32;1m'			# Verde
YEL='\e[33;1m'			# Amarelo
BLU='\e[34;1m'			# Azul
ROS='\e[35;1m'			# Rosa
CYA='\e[36;1m'			# Ciano
NEG='\e[37;1m'			# Negrito
CUI='\e[40;31;5m'		# Vermelho pisacando, aviso!
STD='\e[m'			# Fechamento de cor

# Versão do script
VER="v0.0.6"

## INÍCIO DE FUNÇÕES

# Separação com cor
separacao(){
	for i in {16..21} {21..16} ; do
		echo -en "\e[38;5;${i}m____\e[0m"
	done ; echo
}

# Função pause
pause(){
   read -p "$*"
}

# Verifica se está usando Termux
termux(){
	clear
	echo ""
	separacao
	echo -e " ${ROS}Bem vindo(a) ao script Bugscanner${STD} - ${YEL}$VER${STD}"
	separacao
	echo ""
	pause " Tecle [Enter] para continuar..."
	echo ""
	echo -e " ${ROX063}Verificando dependências, aguarde...${STD}" && sleep 1
	echo ""
	if [ -e "/data/data/com.termux/files/usr/bin/go" ] && [ -e "/data/data/com.termux/files/home/go/bin/subfinder" ] && [ -e "/data/data/com.termux/files/home/go/bin/bugscanner-go" ]; then
		echo -e " ${GRE046}Dependencias encontradas!${STD}"
		echo ""
		pause " Tecle [Enter] para continuar" ; menu_principal
	else
		#echo -e " ${BLU}*${STD} ${NEG}Baixando dependências para utilizar o script, aguarde...${SDT}" && sleep 1
		echo -e " ${BLU}*${STD} ${NEG}Atualizando todo o sistema...${SDT}" && sleep 1
		repo="$HOME/.termux/termux.properties"
        if ! grep -q "apt.default_repo=stable" "$repo"; then
            echo "apt.default_repo=stable" >> "$repo"
        fi
		apt update -y >/dev/null 2>&1 &&
		apt upgrade -y -o Dpkg::Options::=--force-confold >/dev/null 2>&1
		if [ "$?" -eq "0" ]; then
            echo -e " ${GRE}*${STD} ${NEG}Sistema atualizando com sucesso!${SDT}" && sleep 1
            echo ""
            
            # Instala o Fakeroot para rodar o script em um ambiente fakeroot
            echo -e " ${BLU}*${STD} ${NEG}Instalando fakeroot...${SDT}" && sleep 1
            pkg install -y fakeroot >/dev/null 2>&1
            if [ "$?" -eq "0" ]; then
                echo -e " ${GRE}*${STD} ${NEG}Fakeroot instalado com sucesso!${SDT}"
                echo ""
            else
                echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Fakeroot.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
            fi
            
            # Instala o Go
            echo -e " ${BLU}*${STD} ${NEG}Instalando Go...${SDT}" && sleep 1
            pkg install -y golang >/dev/null 2>&1
            if [ "$?" -eq "0" ]; then
                echo -e " ${GRE}*${STD} ${NEG}Go instalado com sucesso!${SDT}"
                echo ""
                
                # Configurando o go para os seus executáveis estaram disponíveis de forma global
                echo -e " ${BLU}*${STD} ${NEG}Configurando Go para funcionar em todos ambientes${SDT}" && sleep 1
                echo 'PATH="$PATH:$HOME/go/bin"' >> $HOME/.bashrc && source $HOME/.bashrc
                if [ "$?" -eq "0" ]; then
                    echo -e " ${GRE}*${STD} ${NEG}Configuração bem sucedida!${SDT}"
                    echo ""
                else
                    echo -e " ${RED}*${STD} ${NEG}Erro ao configurar.\n Limpe as configurações do Termux e volte a executar o script${STD}" ; exit 0
                fi
            else
                echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Go.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
            fi
            
            # Instalando o subfinder
            echo -e " ${BLU}*${STD} ${NEG}Instalando subfinder...${SDT}" && sleep 1
            go install -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest >/dev/null 2>&1
            if [ "$?" -eq "0" ]; then
                echo -e " ${GRE}*${STD} ${NEG}Subfinder instalado com sucesso!${SDT}"
                echo ""
            else
                echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Subfinder.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
            fi
            
            # Instalando o bugscanner
            echo -e " ${BLU}*${STD} ${NEG}Instalando bugscanner...${SDT}" && sleep 1
            go install -v github.com/aztecrabbit/bugscanner-go@latest >/dev/null 2>&1
            if [ "$?" -eq "0" ]; then
                echo -e " ${GRE}*${STD} ${NEG}Bugscanner instalado com sucesso!${SDT}"
            else
                echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Bugscanner.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
            fi
		else
            echo -e " ${RED}*${STD} ${NEG}Erro ao atualizar o sistema.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
		fi
		if [ "$?" -eq "0" ]; then
			clear
			echo ""
			echo -e " ${GRE}*${STD} ${NEG}Instalação conluida com sucesso!${STD}"
			echo ""
			pause " Tecle [Enter] para continuar" ; menu_principal
		else
			echo ""
			echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar as dependências.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
		fi
	fi
}


# Busca bughost para operadora em questão
bugscan(){
    clear
    # Remove a lista existe
    rm lista.txt >/dev/null 2>&1
    echo ""
    echo -e " ${BLU}*${STD} ${NEG}Baixando a lista de domínios...${STD}" && sleep 1
    curl -s https://gitlab.com/talesam/bugscanner/-/raw/main/lista.txt -o lista.txt
    echo ""
    echo -e " ${BLU}*${STD} ${CYA}Antes de continuar, desative o seu WIFI,\n ative os dados móveis da operadora\n e certifique que o chip está sem crédito.${STD}"
    echo ""
    pause " Tecle [Enter] para continuar"
    echo ""

    # Checa se o arruivo foi baixado com sucesso
    if [ "$?" -eq "0" ]; then
        echo ""
        echo -e " ${BLU}*${STD} ${NEG}Digite o nome da operadora para pesquisar:${STD}" && sleep 1
        read operadora
        echo ""
        bugscanner-go scan cdn-ssl --path [scheme] --scheme / --proxy-filename lista.txt -M "GET" -B ${FRONT} --target ${FRONT} -t 64 --timeout 16 -o ${operadora}-front.json

    else
        echo ""
        echo -e " ${RED}*${STD} ${NEG}Erro.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
    fi
}

# Chama as funções
termux
bugscan

