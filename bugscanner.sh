 
#!/bin/bash

# Esse script faz um scanner procurando por Bughost e SNI
# Por Tales A. Mendonça

# ---- NÃO ALTERAR NADA PARA BAIXO

# Versão do script
VER="v0.0.1"

# Definição de Cores
# Tabela de cores: https://misc.flogisoft.com/_media/bash/colors_format/256_colors_fg.png

# Cores degrade
RED001='\e[38;5;1m'		# Vermelho 1
RED009='\e[38;5;9m'		# Vermelho 9
CYA122='\e[38;5;122m'		# Ciano 122
CYA044='\e[38;5;44m'		# Ciano 44
ROX063='\e[38;5;63m'		# Roxo 63
ROX027='\e[38;5;27m'		# Roxo 27
GRE046='\e[38;5;46m'		# Verde 46
GRY247='\e[38;5;247m'		# Cinza 247
LAR208='\e[38;5;208m'		# Laranja 208
LAR214='\e[38;5;214m'		# Laranja 214
AMA226='\e[38;5;226m'		# Amarelo 226
BLU039='\e[38;5;44m'		# Azul 39
MAR094='\e[38;5;94m'		# Marrom 94
MAR136='\e[38;5;136m'		# Marrom 136

# Cores chapadas
CIN='\e[30;1m'			# Cinza
RED='\e[31;1m'			# Vermelho
GRE='\e[32;1m'			# Verde
YEL='\e[33;1m'			# Amarelo
BLU='\e[34;1m'			# Azul
ROS='\e[35;1m'			# Rosa
CYA='\e[36;1m'			# Ciano
NEG='\e[37;1m'			# Negrito
CUI='\e[40;31;5m'		# Vermelho pisacando, aviso!
STD='\e[m'			# Fechamento de cor

# --- Início Funções ---

# Separação com cor
separacao(){
	for i in {16..21} {21..16} ; do
		echo -en "\e[38;5;${i}m____\e[0m"
	done ; echo
}

# Função pause
pause(){
   read -p "$*"
}

# Verifica se está usando Termux
termux(){
	clear
	echo ""
	separacao
	echo -e " ${NEG}Bem vindo(a) ao script Bugscanner${STD}"
	separacao
	echo ""
	pause " Tecle [Enter] para continuar..."
	echo ""
	echo -e " ${ROX063}Verificando dependências, aguarde...${STD}" && sleep 1
	if [ -e "/data/data/com.termux/files/usr/bin/go" ] && [ -e "/data/data/com.termux/files/home/go/bin/subfinder" ] && [ -e "/data/data/com.termux/files/home/go/bin/bugscanner-go" ]; then
		echo -e " ${GRE046}Dependencias encontradas!${STD}"
		echo ""
		pause " Tecle [Enter] para continuar..." ; menu_principal
	else
		#echo -e " ${BLU}*${STD} ${NEG}Baixando dependências para utilizar o script, aguarde...${SDT}" && sleep 1
		echo -e " ${BLU}*${STD} ${NEG}Atualizando todo o sistema...${SDT}" && sleep 1
		pkg update -y -o Dpkg::Options::=--force-confold
		if [ "$?" -eq "0" ]; then
            echo -e " ${GRE}*${STD} ${NEG}Sistema atualizando com sucesso!${SDT}" && sleep 1
            echo -e " ${BLU}*${STD} ${NEG}Instalando dependências...${SDT}" && sleep 1
            
            # Instala o Fakeroot para rodar o script em um ambiente fakeroot
            echo -e " ${BLU}*${STD} ${NEG}Instalando fakeroot...${SDT}" && sleep 1
            pkg install -y fakeroot
            if [ "$?" -eq "0" ]; then
                echo -e " ${GRE}*${STD} ${NEG}Fakeroot instalado com sucesso!${SDT}"
            else
                echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Fakeroot.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
            fi
            
            # Instala o Go
            echo -e " ${BLU}*${STD} ${NEG}Instalando Go...${SDT}" && sleep 1
            pkg install -y golang
            if [ "$?" -eq "0" ]; then
                echo -e " ${GRE}*${STD} ${NEG}Go instalado com sucesso!${SDT}"
                
                # Configurando o go para os seus executáveis estaram disponíveis de forma global
                echo -e " ${BLU}*${STD} ${NEG}Configurando Go para funcionar em todos ambientes${SDT}"
                echo 'PATH="$PATH:$HOME/go/bin"' >> $HOME/.bashrc && source $HOME/.bashrc
                if [ "$?" -eq "0" ]; then
                    echo -e " ${GRE}*${STD} ${NEG}Configuração bem sucedida!${SDT}"
                else
                    echo -e " ${RED}*${STD} ${NEG}Erro ao configurar.\n Limpe as configurações do Termux e volte a executar o script${STD}" ; exit 0
                fi
                
                # Instalando o subfinder
                echo -e " ${BLU}*${STD} ${NEG}Instalando subfinder...${SDT}"
                go install -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest
                if [ "$?" -eq "0" ]; then
                    echo -e " ${GRE}*${STD} ${NEG}Subfinder instalado com sucesso!${SDT}"
                else
                    echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Subfinder.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
                fi
                
                # Instalando o bugscanner
                echo -e " ${BLU}*${STD} ${NEG}Instalando bugscanner...${SDT}"
                go install -v github.com/aztecrabbit/bugscanner-go@latest
                if [ "$?" -eq "0" ]; then
                    echo -e " ${GRE}*${STD} ${NEG}Bugscanner instalado com sucesso!${SDT}"
                else
                    echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar Bugscanner.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
                fi
            else
                echo -e " ${RED}*${STD} ${NEG}Erro ao instalar Go.\n Limpe as configurações do Termux e volte a executar o script${STD}" ; exit 0
            fi
		else
            echo -e " ${RED}*${STD} ${NEG}Erro ao atualizar o sistema.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
		fi
		if [ "$?" -eq "0" ]; then
			echo ""
			echo -e " ${GRE}*${STD} ${NEG}Instalação conluida com sucesso!${STD}"
			echo ""
			pause " Tecle [Enter] para continuar..." ; menu_principal
		else
			echo ""
			echo -e " ${RED}*${STD} ${NEG}Erro ao baixar e instalar as dependências.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
		fi
	fi
}

### Início configuração apps

# Função para encontrar subdomínios com base em um domínio
encontrar_subs(){
	echo -e " ${CYA}*${STD} ${NEG}Para utilizar este recurso é necessário ter dados${STD}"
	echo ""
	echo -e " ${BLU}*${STD} ${NEG}Entre com o domínio para pesquisar subdomínios${STD}"
	read DOMINIO
	echo -e " ${BLU}*${STD} ${NEG}Executando o comando, aguarde a conlcusão...${STD}" && sleep 1
	subfinder -d $DOMINIO -all -timeout 10 -t 512 -o subdominios.txt
	if [ "$?" -eq "0" ]; then
		echo -e " ${GRE}*${STD} ${NEG}Comando executado com sucesso!\n Verifique o arquivo subdominios.txt${STD}"
	else
		echo -e " ${RED}*${STD} ${NEG}Erro ao atualizar o sistema.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
	fi
}

# Função para encontrar subdomínios com base em uma lista de domínios
lista-dominio(){
	echo -e " ${CYA}*${STD} ${NEG}Para utilizar este recurso é necessário ter dados${STD}"
	echo ""
	echo -e " ${BLU}*${STD} ${NEG}Será aberto o editor${STD} ${ROS}nano${STD} ${NEG}para que copie a lista\n dos domínios.${STD}"
	echo -e " ${BLU}*${STD} ${NEG}Para salvar tecle [${STD}${YEL}CTRL${STD}${NEG}]+[${STD}${YEL}x${STD}${NEG}] e depois confirme com ${YEL}y${STD}${NEG}.${STD}"
	nano dominiolista.txt
	echo -e " ${BLU}*${STD} ${NEG}Executando o comando, aguarde a conlcusão...${STD}" && sleep 1
	subfinder -dL dominiolista.txt -all -timeout 3 -t 756 -o SaidaDominioLista.txt
	if [ "$?" -eq "0" ]; then
		echo -e " ${GRE}*${STD} ${NEG}Comando executado com sucesso!\n Verifique o arquivo SaidaDominioLista.txt${STD}"
	else
		echo -e " ${RED}*${STD} ${NEG}Erro ao atualizar o sistema.\n Verifique sua conexão e tente novamente.${STD}" ; exit 0
	fi
	rm dominiolista.txt
}

# https://pst.klgrth.io/


# exibe menu principal em cores
menu_principal() {
	clear
	option=0
	until [ "$option" = "3" ]; do
		separacao
		echo -e "${ROS}BUGSCAN${STD} - ${YEL}$VER${STD}$"
		separacao
		echo ""
		echo -e " ${BLU}1.${STD} ${RED009}Subfinder${STD}"
		echo -e " ${BLU}2.${STD} ${GRY247}Bugscanner${STD}"
		echo -e " ${BLU}0.${STD} ${RED}Sair${STD}"
		echo ""
		read -p " Digite um número:" option
		case $option in
			1 ) menu_subfinder ;;
			2 ) menu_bugscanner ;;
			0 ) exit ;;
			* ) clear; echo -e " ${NEG}Por favor escolha${STD} ${ROS}1${STD}${NEG},${STD} ${ROS}2${STD} ${NEG}ou${STD} ${ROS}0 para sair${STD}";
		esac
	done
}

# Menu Bugscanner
menu_subfinder(){ 
	clear
	option=0
	until [ "$option" = "3" ]; do
		separacao
		echo -e " ${ROX027}Subfinder${STD}"
		separacao
		echo ""
		echo -e " ${BLU}1.${STD} ${GRY247}Encontrar sub-domínios${STD}"
		echo -e " ${BLU}2.${STD} ${GRE046}Lista de domínios${STD}"
		echo -e " ${BLU}0.${STD} ${ROX063}Retornar ao Menu Principal${STD}"
		echo ""
		read -p " Digite um número:" option
		case $option in
			1 ) encontrar_subs ;;
			2 ) lista-dominio ;;
			0 ) menu_principal ;;
			* ) clear; echo -e " ${NEG}Por favor escolha${STD} ${ROS}1${STD}${NEG},${STD} ${ROS}2${STD}${NEG},${STD} ${NEG}ou${STD} ${ROS}3${STD}${NEG}";
		esac
	done
}

# Menu Bugscanner
menu_bugscanner(){ 
	clear
	option=0
	until [ "$option" = "3" ]; do
		separacao
		echo -e " ${ROX027}Bugscanner${STD}"
		separacao
		echo ""
		echo -e " ${BLU}1.${STD} ${GRY247}CDN-SSL ${STD}"
		echo -e " ${BLU}2.${STD} ${GRE046}DIRECT${STD}"
		echo -e " ${BLU}3.${STD} ${GRE046}SNI${STD}"
		echo -e " ${BLU}0.${STD} ${ROX063}Retornar ao Menu Principal${STD}"
		echo ""
		read -p " Digite um número:" option
		case $option in
			1 ) cdn-ssl ;;
			2 ) direct ;;
			3 ) sni ;;
			0 ) menu_principal ;;
			* ) clear; echo -e " ${NEG}Por favor escolha${STD} ${ROS}1${STD}${NEG},${STD} ${ROS}2${STD}${NEG},${STD} ${ROS}3${STD}${NEG},${STD} ${NEG}ou${STD} ${ROS}3${STD}${NEG}";
		esac
	done
}

